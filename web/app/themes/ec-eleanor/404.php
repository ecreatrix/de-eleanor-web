<?php
/**
 * The template for displaying 404 pages (not found).
 *
 */

get_header();
?>

<?php do_action('ec_before_page');?>

<div class="page-content jumbotron"><div class="container">

	<p><?php esc_html_e('It looks like nothing was found at this location. Try one of the links below or a search.', eC\Theme\SHORTNAME);?></p>

	<?php get_search_form();?>

	<?php the_widget('WP_Widget_Recent_Posts');?>

<?php $archive_content = '<p>' . sprintf(esc_html__('Try looking in the monthly archives', eC\Theme\SHORTNAME)) . '</p>';

the_widget('WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content");

the_widget('WP_Widget_Tag_Cloud');
?>

						</div><!-- .content --></div><!-- .page-content -->
<?php
do_action('ec_after_page');

get_footer();?>
