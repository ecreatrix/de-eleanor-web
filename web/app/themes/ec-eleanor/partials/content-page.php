<?php
/**
 * The template used for displaying page content in page.php
 *
 */
?>

	<?php do_action('ec_before_page');
the_content();

wp_link_pages([
    'before' => '<div class="page-links">' . __('Pages:', 'ec_theme'),
    'after'  => '</div>',
]);
do_action('ec_after_page');
?>
