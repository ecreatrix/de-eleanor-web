<?php
/**
 *
 *
 */
use \eC\Theme as Theme;

$content_class = apply_filters( 'ec_post_content_class', 'post-content' );

$title      = '<h1 class="page-title no-lines h2">' . apply_filters( 'ec_post_title', get_the_title() ) . '</h1>';
$date       = '<div class="date">' . apply_filters( 'ec_post_date', get_the_date() ) . '</div>';
$categories = Theme\Sections\Frontend::single_cats();
$content    = '<div class="' . $content_class . '">' . apply_filters( 'the_content', get_the_content() ) . '</div>';

$id = $post->ID;

$image         = Theme\Posts\Helpers::default_image( $id, false, 'large' );
$show_featured = get_post_meta( $id, 'ec_show_featured', true );

if ( $image && 'false' != $show_featured ) {
	echo '<div class="wp-block-ectheme-jumbotron jumbotron has-custom-bg has-image-bg image-bg-active" style="background-image:url(\'' . $image . '\')"></div>';
}

echo '<div class="jumbotron"><div class="container pb-0">' . $title . '</div></div>';

echo $content;

echo Theme\Posts\Blog::post_nav();
