.'<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */

$jumbotron = new eC\BSShortcodes\Shortcodes\Jumbotron();
$title     = '<h1 class="page-title h2">' . __('Nothing Found', 'ec_theme') . '</h1>';

$search = get_search_form(false);

if (current_user_can('publish_posts')) {
    $output = '<p class="text-center mb-5">' . printf(__('Ready to publish your first post? <a href="%1$s">Get started here.</a>', 'ec_theme'), esc_url(admin_url('post-new.php'))) . '</p>';

    $search = '';
} elseif (is_search()) {
    $output = '<p class="text-center mb-5">' . __('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'ec_theme') . '</p>';

} else {
    $output = '<p class="text-center mb-5">' . __('It seems we can\'t find what you\'re looking for. Perhaps searching can help.', 'ec_theme') . '</p>';
}

echo $jumbotron->callback([], $title . $output . $search);