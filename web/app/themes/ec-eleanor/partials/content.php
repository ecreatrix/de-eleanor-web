<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package ec-theme
 */

the_title(sprintf('<h2 class="h4 entry-title"><a href="%s" rel="bookmark">', esc_url(get_permalink())),
    '</a></h2>');?>

	<?php echo get_the_post_thumbnail($post->ID, 'large'); ?>

		<?php
the_excerpt();
?>

		<?php
wp_link_pages([
    'before' => '<div class="page-links">' . __('Pages:', 'understrap'),
    'after'  => '</div>'
]);
?>
