<?php
/**
 * The template part for displaying results in search pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */
?>

<div class="my-6">
	<h6 class="entry-title">
		<a href="<?php echo esc_url(get_permalink()); ?>" rel="bookmark">
			<?php Frontend::search_title_highlight();?>
		</a>
	</h6>

	<div class="entry-summary">
		<?php \eC\Theme\Sections\Frontend::search_excerpt_highlight();?>
	</div><!-- .entry-summary -->
</div>