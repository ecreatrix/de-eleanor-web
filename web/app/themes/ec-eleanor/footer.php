<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 */
?>

	<?php do_action('ec_after_article');?>
	</article><!-- #post-## -->
	</div><!-- .site-content -->
	<?php do_action('ec_before_footer');?>
	<footer id="colophon" class="site-footer jumbotron position-sticky">
		<?php
do_action('ec_footer');
?>
	</footer><!-- #colophon -->
<?php wp_footer();
do_action('ec_after_footer');
?>

</body>
</html>
