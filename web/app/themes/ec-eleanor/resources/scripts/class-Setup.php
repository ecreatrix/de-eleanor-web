<?php
namespace eC\Theme\Scripts;

use eC\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Setup::class ) ) {
    class Setup {
        public function __construct() {
            $this->init_enqueues();
        }

        // Default WP enqueue/register script functions
        public static function add_localized_script_enqueue( $handle = '', $name = '', $parameters = [] ) {
            wp_localize_script( $handle, $name, $parameters );
        }

        // Add ajax parameters to script
        /* $scripts = [
        'handle'     => script handle,
        'name'         => name used for ajax parameter access,
        'parameters' => array/string of values for ajax use
        ],
         */
        public static function add_localized_scripts( $scripts ) {
            // If $scripts isn't an array or is empty, stop.
            if ( ! is_array( $scripts ) || empty( $scripts ) ) {
                return;
            }

            $default_script = ['handle' => '', 'name' => '', 'parameters' => ''];

            foreach ( $scripts as $script ) {
                $script = array_merge( $default_script, $script );

                self::add_localized_script_enqueue( $script['handle'], $script['name'], $script['parameters'] );
            }
        }

        // Default WP enqueue/register functions
        public static function add_script_enqueue( $name = '', $filename = 'assets/scripts/main.min.js', $dependencies = [], $enqueue = true ) {
            $reload   = self::get_file_time( $filename );
            $filename = self::get_file_path( $filename );

            if ( ! $filename ) {
                return;
            }

            if ( $enqueue ) {
                wp_enqueue_script( $name, $filename, $dependencies, $reload, true );
            } else {
                wp_register_script( $name, $filename, $dependencies, $reload, true );
            }
        }

        // Enqueue scripts
        /* $scripts = [
        'name'         => name used for registering,
        'filename'     => filename for in plugin/theme, url for outside,
        'dependencies' => array of script names that should load first,
        'enqueue'      => true to enqueue, false just to register,
        'footer'      => true to add to footer, false for header,
        ],
         */
        public static function add_scripts( $scripts ) {
            // If $scripts isn't an array or is empty, stop.
            if ( ! is_array( $scripts ) || empty( $scripts ) ) {
                return;
            }

            $default_script = ['name' => '', 'filename' => '', 'dependencies' => '', 'enqueue' => true, 'footer' => true];

            foreach ( $scripts as $script ) {
                $script = array_merge( $default_script, $script );

                self::add_script_enqueue( $script['name'], $script['filename'], $script['dependencies'], $script['enqueue'] );
            }
        }

        // Default WP enqueue/register script functions
        public static function add_style_enqueue( $name = '', $filename = 'assets/styles/main.min.css', $dependencies = [], $enqueue = true ) {
            $reload   = self::get_file_time( $filename );
            $filename = self::get_file_path( $filename );

            if ( ! $filename ) {
                return;
            }

            if ( $enqueue ) {
                wp_enqueue_style( $name, $filename, $dependencies, $reload );
            } else {
                wp_register_style( $name, $filename, $dependencies, $reload );
            }
        }

        // Enqueue styles
        /* $styles = [
        'name'         => name used for registering,
        'filename'     => filename for in plugin/theme, url for outside,
        'dependencies' => array of style names that should load first,
        'enqueue'      => true to enqueue, false just to register,
        ],
         */
        public static function add_styles( $styles ) {
            // If $styles isn't an array or is empty, stop.
            if ( ! is_array( $styles ) || empty( $styles ) ) {
                return;
            }

            $default_style = ['name' => '', 'filename' => '', 'dependencies' => '', 'enqueue' => ''];

            foreach ( $styles as $style ) {
                $style = array_merge( $default_style, $style );

                self::add_style_enqueue( $style['name'], $style['filename'], $style['dependencies'], $style['enqueue'] );
            }
        }

        public function enqueues() {
            $this->add_styles( $this->styles );

            $this->add_scripts( $this->scripts );
        }

        public static function get_file_path( $filename ) {
            $path = Theme\PATH;
            $uri  = Theme\URI;

            if ( strpos( $filename, 'http' ) !== false ) {
                //echo '<br>found: ' . $filename;

                return $filename;
            } else if ( file_exists( $path . $filename ) ) {
                //echo '<br>found: ' . $uri . $filename;

                return $uri . $filename;
            }

            //echo '<br>file not found: ' . $uri . $filename;

            return false;
        }

        public static function get_file_time( $filename ) {
            $path = Theme\PATH;
            $uri  = Theme\URI;

            // Get file reload. Plugin/Theme version for external scripts and file edit time for internal scripts
            $reload = Theme\VERSION;

            if ( strpos( $filename, 'http' ) === false && file_exists( $path . $filename ) ) {
                $reload = filemtime( $path . $filename );
            }

            return $reload;
        }

        public function init_enqueues() {
            if ( class_exists( Enqueues::class ) ) {
                new Enqueues();
            }
        }
    }
}
