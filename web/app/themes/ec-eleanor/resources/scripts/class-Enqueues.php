<?php
namespace eC\Theme\Scripts;

use eC\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Enqueues::class ) ) {
    class Enqueues {
        public function __construct() {
            // General scripts and styles
            add_action( 'wp_enqueue_scripts', [$this, 'frontend_enqueues'], 99 );

            // WP scripts and styles
            add_action( 'admin_enqueue_scripts', [$this, 'wp_enqueues'], 99 );
        }

        public static function admin_assets() {
            $styles = [
                'name' => [
                    'name'         => Theme\SHORTNAME . 'admin-styles',
                    'filename'     => 'assets/styles/admin.min.css',
                    'dependencies' => [],
                    'enqueue'      => true,
                ],
            ];

            Setup::add_styles( $styles );
        }

        public static function fonts_assets() {
            $script = [
                [
                    'name'     => 'ec-fontawesome',
                    'filename' => 'https://use.fontawesome.com/0a60f349fc.js',
                ],
            ];

            Setup::add_scripts( $script );

            $styles = [
                [
                    'name'     => 'ec-fontawesome',
                    'filename' => 'https://use.fontawesome.com/releases/v5.2.0/css/all.css',
                    'enqueue'  => true,
                ],
            ];

            Setup::add_styles( $styles );
        }

        public function frontend_enqueues() {
            $this->project_assets();

            $this->fonts_assets();
        }

        public static function project_assets() {
            $scripts = [
                'vendor'      => [
                    'name'         => Theme\SHORTNAME . 'vendors',
                    'filename'     => 'assets/scripts/vendors.min.js',
                    'dependencies' => ['jquery'],
                ],
                'main-script' => [
                    'name'         => Theme\SHORTNAME . 'main-script',
                    'filename'     => 'assets/scripts/main.min.js',
                    'dependencies' => ['jquery', Theme\SHORTNAME . 'vendors'],
                ],
            ];

            Setup::add_scripts( $scripts );

            $styles = [
                'name' => [
                    'name'         => Theme\SHORTNAME . 'main-styles',
                    'filename'     => 'assets/styles/main.min.css',
                    'dependencies' => [],
                    'enqueue'      => true,
                ],
            ];

            Setup::add_styles( $styles );
        }

        public function wp_enqueues() {
            $this->admin_assets();
            $this->fonts_assets();
        }
    }
}