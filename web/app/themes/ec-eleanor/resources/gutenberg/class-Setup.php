<?php
namespace eC\Theme\Gutenberg;

use eC\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Setup::class ) ) {
    defined( __NAMESPACE__ . '\FRONTEND_STYLE' ) or define( __NAMESPACE__ . '\FRONTEND_STYLE', Theme\SHORTNAME . 'gutenberg-frontend-styles' );
    defined( __NAMESPACE__ . '\EDITOR_STYLE' ) or define( __NAMESPACE__ . '\EDITOR_STYLE', Theme\SHORTNAME . 'gutenberg-editor-styles' );
    defined( __NAMESPACE__ . '\SCRIPT' ) or define( __NAMESPACE__ . '\SCRIPT', Theme\CODENAME . 'gutenberg' );
    class Setup {
        public function __construct() {
            $this->init_classes();

            add_filter( 'block_categories', [$this, 'block_categories'] );

            add_filter( Theme\BLOCK . '_icons_list', [$this, 'edit_icons'], 13, 1 );
        }

        public function block_categories( $categories ) {
            $categories = array_merge(
                [
                    [
                        'slug'  => 'ecgttheme',
                        'title' => __( 'Theme', Theme\SHORTNAME ),
                    ],
                ],
                $categories
            );

            return $categories;
        }

        public static function default_gutenberg_attributes() {
            $attributes = [
                'ecBlockId' => [
                    'type'    => 'string',
                    'default' => false,
                ],
                //'className' => [
                //    'type'    => 'string',
                //    'default' => ''
                //]
            ];

            return $attributes;
        }

        public static function default_scheme_attributes() {
            $attributes = [
            ];

            return $attributes;
        }

        public function init_classes() {
            $this->init_classes_wp();

            $this->init_classes_blocks();
        }

        public function init_classes_blocks() {
        }

        public function init_classes_wp() {
            if ( class_exists( Prettify::class ) ) {
                new Prettify();
            }
        }
    }
}
