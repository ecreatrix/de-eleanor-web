<?php
namespace eC\Theme\Gutenberg;

use eC\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Prettify::class ) ) {
    class Prettify {
        public function __construct() {
            add_action( 'after_setup_theme', [$this, 'disable_fonts'] );

            add_filter( 'allowed_block_types', [$this, 'limit_block_types'] );
        }

        public function core_blocks() {
            $blocks = [
                //'core/archives',
                //'core/audio',
                'core/block',
                'core/button',
                //'core/categories',
                'core/code',
                //'core/column',
                //'core/columns',
                'core/cover',
                //'core/embed',
                //'core/file',
                //'core/freeform',
                'core/gallery',
                'core/group',
                'core/heading',
                //'core/html',
                'core/image',
                //'core/latestComments',
                //'core/latestPosts',
                'core/list',
                //'core/more',
                //'core/nextpage',
                'core/paragraph',
                //'core/preformatted',
                //'core/pullquote',
                //'core/quote',
                'core/reusableBlock',
                //'core/separator',
                'core/shortcode',
                //'core/spacer',
                //'core/subhead',
                'core/table',
                //'core/textColumns',
                //'core/verse',
                //'core/video',
            ];

            return $blocks;
        }

        public function disable_fonts() {
            add_theme_support( 'disable-custom-font-sizes' );
            // forces the dropdown for font sizes to only contain "normal"

            add_theme_support( 'editor-font-sizes', [
                [
                    'name' => 'Normal',
                    'size' => 16,
                    'slug' => 'normal',
                ],
            ] );
        }

        public function limit_block_types( $allowed_block_types ) {
            $core_blocks = $this->core_blocks();
            //$bssgt_blocks   = $this->ectheme_blocks();
            //$theme_blocks   = $this->theme_blocks();
            $plugins_blocks = $this->plugins_blocks();

            //TODO add filter to remove icons not selected in settings

            return array_merge( $core_blocks, $plugins_blocks );
        }

        public function plugins_blocks() {
            $blocks = [
            ];

            return $blocks;
        }
    }
}
