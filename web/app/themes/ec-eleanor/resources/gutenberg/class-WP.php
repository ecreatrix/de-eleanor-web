<?php
namespace eC\Theme\Gutenberg;

use eC\Theme as Theme;

/*
 * Rest API functions
 *
 * @package @@ectheme
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( WP::class ) ) {
	class WP {
		public function __construct() {
		}

		/**
		 * Get attachment image <img> tag.
		 *
		 * @param  WP_REST_Request $request request object.
		 * @return mixed
		 */
		//FIX to force WP_REST_Request  type parameters
		public function get_attachment_image( $request ) {
			$id   = $request->get_param( 'id' );
			$size = $request->get_param( 'size' );
			$icon = $request->get_param( 'icon' );
			$attr = $request->get_param( 'attr' );

			$image = wp_get_attachment_image( $id, $size, $icon, $attr );

			if ( $image ) {
				return Theme\Scripts\Rest::success( $image );
			} else {
				return Theme\Scripts\Rest::error( 'no_image_found', __( 'Image not found.', '@@text_domain' ) );
			}
		}

		/**
		 * Get attachment image <img> tag permissions.
		 *
		 * @param  WP_REST_Request $request request object.
		 * @return bool
		 */
		//FIX to force WP_REST_Request  type parameters
		public function get_attachment_image_permission( $request ) {
			$id = $request->get_param( 'id' );

			if ( ! $id ) {
				return Theme\Scripts\Rest::error( 'no_id_found', __( 'Provide image ID.', '@@text_domain' ) );
			}

			return true;
		}

		/**
		 * Get custom code.
		 *
		 * @return mixed
		 */
		public function get_custom_code() {
			$custom_code = get_option( Theme\CODENAME . 'custom_code', [] );

			if ( is_array( $custom_code ) ) {
				return Theme\Scripts\Rest::success( $custom_code );
			} else {
				return Theme\Scripts\Rest::error( 'no_custom_code', __( 'Custom code not found.', '@@text_domain' ) );
			}
		}

		/**
		 * Get read custom code permissions.
		 *
		 * @return bool
		 */
		public function get_custom_code_permission() {
			if ( ! current_user_can( 'edit_theme_options' ) ) {
				return Theme\Scripts\Rest::error( 'user_dont_have_permission', __( 'User don\'t have permissions to change options.', '@@text_domain' ) );
			}

			return true;
		}

		/**
		 * Get customizer data.
		 *
		 * @return mixed
		 */
		public function get_customizer() {
			$options = get_option( Theme\CODENAME . 'customizer_fields' );

			if ( ! empty( $options ) ) {
				return Theme\Scripts\Rest::success( $options );
			} else {
				return Theme\Scripts\Rest::error( 'no_options_found', __( 'Options not found.', '@@text_domain' ) );
			}
		}

		/**
		 * Get read customizer permissions.
		 *
		 * @return bool
		 */
		public function get_customizer_permission() {
			if ( ! current_user_can( 'edit_theme_options' ) ) {
				return Theme\Scripts\Rest::error( 'user_dont_have_permission', __( 'User don\'t have permissions to change Customizer options.', '@@text_domain' ) );
			}

			return true;
		}

		/**
		 * Update Disabled Blocks.
		 *
		 * @param  WP_REST_Request $request request object.
		 * @return mixed
		 */
		public function update_disabled_blocks( $request ) {
			$new_settings = $request->get_param( 'blocks' );
			$updated      = '';

			if ( is_array( $new_settings ) ) {
				$key              = Theme\CODENAME . 'disabled_blocks';
				$current_settings = get_option( $key, [] );
				$updated          = update_option( $key, $current_settings, $new_settings );
			}

			if ( ! empty( $updated ) ) {
				return $this->success( true );
			} else {
				return $this->error( 'no_disabled_blocks_updated', __( 'Failed to update disabled blocks.', '@@text_domain' ) );
			}
		}

		public static function update_options( $name, $options ) {
			if ( is_array( $options ) ) {
				$options = md5(
					json_encode( $options )
				);
			}

			$updated = update_option( $name, $options );

			if ( ! empty( $updated ) ) {
				return Theme\Scripts\Rest::success( $updated );
			} else {
				return Theme\Scripts\Rest::error( 'no_options_found', __( 'Failed to update option.', '@@text_domain' ) );
			}
		}

		/**
		 * Get update settings permissions.
		 *
		 * @return bool
		 */
		public function update_options_permission() {
			//if ( ! current_user_can( 'manage_options' ) ) {
			//    return Theme\Scripts\Rest::error( 'user_dont_have_permission', __( 'User don\'t have permissions to change options.', '@@text_domain' ) );
			//}

			return true;
		}

		/**
		 * Update Settings.
		 *
		 * @param  WP_REST_Request $request request object.
		 * @return mixed
		 */
		//FIX allow (WP_REST_Request
		public function update_settings( $request ) {
			$new_settings = $request->get_param( 'settings' );
			$this->update_options( Theme\CODENAME . 'settings', $new_settingsوtrue );
		}
	}
}
