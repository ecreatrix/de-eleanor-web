<?php
/**
 * Custom code plugin.
 *
 * @package ecBssGt
 */

namespace eC\Theme\Gutenberg;

use eC\Theme as Plugin;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( CustomCode::class ) ) {
    class CustomCode {
        public function __construct() {
            add_action( 'init', [$this, 'register_meta'] );
        }

        /**
         * Register meta.
         */
        public function register_meta() {
            register_meta( 'post', Theme\CODENAME . 'custom_css', [
                'show_in_rest' => true,
                'single'       => true,
                'type'         => 'string',
            ] );
            register_meta( 'post', Theme\CODENAME . 'custom_js_head', [
                'show_in_rest' => true,
                'single'       => true,
                'type'         => 'string',
            ] );
            register_meta( 'post', Theme\CODENAME . 'custom_js_foot', [
                'show_in_rest' => true,
                'single'       => true,
                'type'         => 'string',
            ] );
        }
    }
}
