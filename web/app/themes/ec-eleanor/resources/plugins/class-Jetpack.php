<?php
/**
 * Jetpack Compatibility File
 * See: http://jetpack.me/
 *
 */

namespace eC\Theme\Plugins;

// Exit if accessed directly.
if (!defined('ABSPATH')) {exit;}

if (!class_exists(Jetpack::class)) {
    class Jetpack
    {
        public function __construct()
        {
            add_action('after_setup_theme', [$this, 'setup']);
        }

        // Custom render function for Infinite Scroll.
        public function infinite_scroll_render()
        {
            while (have_posts()) {
                the_post();
                if (is_search()):
                    get_template_part('loop-templates/content', 'search');
                else:
                    get_template_part('loop-templates/content', get_post_format());
                endif;
            }
        }

        public function setup()
        {
            // Add theme support for Responsive Videos.
            add_theme_support('jetpack-responsive-videos');

            // Add theme support for Social Menus
            add_theme_support('jetpack-social-menu');

            add_theme_support('infinite-scroll', [
                'container' => 'main',
                'render'    => 'components_infinite_scroll_render',
                'footer'    => 'page',
            ]);

            $this->infinite_scroll_render();
            $this->social_menu();
        }

        public function social_menu()
        {
            if (!function_exists('jetpack_social_menu')) {
                return;
            } else {
                jetpack_social_menu();
            }
        }
    }
}
