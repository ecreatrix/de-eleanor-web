<?php
namespace eC\Theme\Plugins;

use eC\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Piklist::class ) ) {
    class Piklist {
        public function __construct() {
            add_filter( 'piklist_admin_pages', [$this, 'admin_pages'] );

            add_action( 'admin_init', [$this, 'register_company_info'] );
        }

        // Custom render function for Infinite Scroll.
        public function admin_pages( $pages ) {
            $pages[] = [
                'page_title' => 'Eleanor Fulcher',
                'menu_title' => 'Eleanor',
                'menu_slug'  => 'company_info',

                //'sub_menu'   => 'options-general.php',

                'menu_icon'  => Theme\URI . 'assets/images/favicon.svg',
                'page_icon'  => Theme\URI . 'assets/images/favicon.svg',

                'capability' => 'manage_options',
                'position'   => 2,
                'setting'    => 'ec_company_info_settings',

                'save_text'  => 'Save settings',
            ];

            return $pages;
        }

        // Register settings using the Settings API
        public function register_company_info( $whitelist_options ) {
            register_setting( 'company_info', 'ec_socialmedia' );
        }
    }
}
