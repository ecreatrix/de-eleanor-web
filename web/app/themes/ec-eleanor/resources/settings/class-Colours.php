<?php
namespace eC\Theme\Settings;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Colours::class ) ) {
    class Colours {
        public $black = '#000';

        public $grey100 = '#F8F9FA';

        public $primary = '#CE2E2D';

        public $tertiary = '#000620';

        public $white = '#FFF';

        public function __construct() {
            add_action( 'after_setup_theme', [$this, 'colour_palette'] );
            //add_action('after_setup_theme', [$this, 'custom_colours']);
            add_action( 'after_setup_theme', [$this, 'disable_colors'] );
        }

        public function colour_palette() {
            add_theme_support( 'editor-color-palette', $this->colour_palette_options() );
        }

        // Add admin menu.
        public function colour_palette_options() {
            $colours = [
                [
                    'name'  => esc_html__( 'Primary', '@@textdomain' ),
                    'slug'  => 'primary',
                    'color' => $this->primary,
                ],
                [
                    'name'  => esc_html__( 'Tertiary', '@@textdomain' ),
                    'slug'  => 'tertiary',
                    'color' => $this->tertiary,
                ],
                [
                    'name'  => esc_html__( 'Grey', '@@textdomain' ),
                    'slug'  => 'grey-100',
                    'color' => $this->grey100,
                ],
                [
                    'name'  => esc_html__( 'White', '@@textdomain' ),
                    'slug'  => 'white',
                    'color' => $this->white,
                ],
                [
                    'name'  => esc_html__( 'No Color', '@@textdomain' ),
                    'slug'  => 'transparent',
                    'color' => 'transparent',
                ],
            ];

            return $colours;
        }

        /**
         * Render the admin page.
         */
        public function custom_colours() {
            add_theme_support( 'disable-custom-colors' );
        }

        public function disable_colors() {
                                                        //TODO enable custom colors
                                                        //add_theme_support( 'editor-color-palette' );  // Disable all colours
            add_theme_support( 'disable-custom-colors' ); // Disable custom colours
        }
    }
}
