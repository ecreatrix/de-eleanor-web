<?php
namespace eC\Theme\Settings;

use eC\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Setup::class ) ) {
    class Setup {
        public function __construct() {
            $this->init_colours();
            $this->init_page();
            // Add the options page and menu item.
            //add_action('admin_menu', [$this, 'admin_menu']);
        }

        /**
         * Add admin menu.
         */
        public function admin_menu() {
            add_menu_page(
                esc_html__( 'Eleanor', 'ec_theme' ),
                esc_html__( 'Eleanor', 'ec_theme' ),
                'manage_options',
                'company_info',
                [$this, 'display_admin_page'],
                Theme\URI . 'assets/images/company-menu-icon.svg',
                2
            );
        }

        /**
         * Render the admin page.
         */
        public function display_admin_page() {
            echo '<div class="company_info-admin-page"></div>';
        }

        public function init_colours() {
            if ( class_exists( Colours::class ) ) {
                new Colours();
            }
        }

        public function init_page() {
            if ( class_exists( Page::class ) ) {
                new Page();
            }
        }
    }
}
