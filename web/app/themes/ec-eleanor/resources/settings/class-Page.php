<?php
namespace eC\Theme\Settings;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Page::class ) ) {
    class Page {
        public function __construct() {
        }

        /**
         * Add admin menu.
         */
        public function admin_menu() {
            add_menu_page(
                esc_html__( 'Theme Blocks', Theme\BLOCK ),
                esc_html__( 'Theme Blocks', Theme\BLOCK ),
                'manage_options',
                Theme\BLOCK,
                [$this, 'display_admin_page'],
                'dashicons-admin-' . Theme\SHORTNAME,
                105
            );

            add_submenu_page(
                Theme\BLOCK,
                '',
                esc_html__( 'Blocks', Theme\BLOCK ),
                'manage_options',
                Theme\BLOCK
            );
            add_submenu_page(
                Theme\BLOCK,
                '',
                esc_html__( 'Settings', Theme\BLOCK ),
                'manage_options',
                'admin.php?page=' . Theme\BLOCK . '&sub_page=settings'
            );
        }

        /**
         * Render the admin page.
         */
        public function display_admin_page() {
            echo '<div class="' . Theme\BLOCK . '-admin-page"></div>';
        }
    }
}
