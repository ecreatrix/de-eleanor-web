<?php
namespace eC\Theme\WP;

use eC\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( Settings::class ) ) {
    class Settings {
        public function __construct() {
            //add_action( 'admin_menu', [$this, 'add_admin_menu'] );
            //add_action( 'admin_init',  [$this, 'settings_init'] );
        }

        public function add_admin_menu() {
            add_menu_page(
                __( 'Company info', 'ec_theme' ),
                'Eleanor',
                'manage_options',
                'company_info',
                'ec_company_info_settings',
                Theme\URI . 'assets/images/company-menu-icon.svg',
                2
            );
        }

        public function settings_init() {
            register_setting( 'pluginPage', 'ec_settings' );

            add_settings_section(
                'ec_pluginPage_section',
                __( 'Your section description', 'ectheme' ),
                'ec_settings_section_callback',
                'pluginPage'
            );

            add_settings_field(
                'ec_text_field_0',
                __( 'Settings field description', 'ectheme' ),
                'ec_text_field_0_render',
                'pluginPage',
                'ec_pluginPage_section'
            );

            add_settings_field(
                'ec_text_field_1',
                __( 'Settings field description', 'ectheme' ),
                'ec_text_field_1_render',
                'pluginPage',
                'ec_pluginPage_section'
            );

        }
    }
}
