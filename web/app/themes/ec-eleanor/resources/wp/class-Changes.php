<?php
namespace eC\Theme\WP;

use eC\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Changes::class ) ) {
    class Changes {
        public function __construct() {
            add_filter( 'the_content', [$this, 'filter_ptags_on_images'], 999 );

            // Replace WP link with Detale in footer
            add_filter( 'admin_footer_text', [$this, 'change_admin_footer'] );

            $this->setup_favicons();
            $this->setup_tinymce();
            $this->setup_login();
            $this->enabled_featured_images();

            add_filter( 'excerpt_more', [$this, 'custom_excerpt_more'] );
            add_filter( 'wp_trim_excerpt', [$this, 'all_excerpts_get_more_link'] );

            // Make theme available for translation. Translations can be filed in the /languages/ directory.
            //load_theme_textdomain(eC\Theme\SHORTNAME, get_template_directory() . '/languages');

            // Create transient to store cats list for recent post shortcode
            add_action( 'transition_post_status', [$this, 'publish_new_post'], 10, 3 );

            // To allow piklist to work
            add_action( 'init', [$this, 'custom_fields_work'] );
        }

        public function add_admin_style() {
            wp_enqueue_style( 'ec-bootstrap-admin', Theme\URI . 'assets/styles/style-admin.css' );
            wp_enqueue_style( 'ec-fontawesome', Theme\FONTAWESOME );
        }

        public function add_editor_style() {
            $screen = get_current_screen();
            if ( 'post' !== $screen->base || 'page' !== $screen->post_type ) {
                return;
            }

            $font_url = str_replace( ',', '%2C', Theme\GOOGLEFONT );

            add_editor_style( $font_url );
            add_editor_style( Theme\URI . 'assets/styles/style-editor.css' );
        }

        public function add_login_style() {
            wp_enqueue_style( 'ec-bootstrap-login', Theme\URI . 'assets/styles/admin.min.css' );
            wp_enqueue_script( 'ec-bootstrap-login', Theme\URI . 'assets/styles/scripts/editor-scripts.min.js', ['jquery'], '1.0', true );

            wp_enqueue_style( 'ec-child-google-fonts' );
            wp_enqueue_script( 'ec-child-bootstrap' );
        }

        /**
         * Adds a custom read more link to all excerpts, manually or automatically generated
         *
         * @param  string   $post_excerpt Posts's excerpt.
         * @return string
         */
        public function all_excerpts_get_more_link( $post_excerpt ) {
            return $post_excerpt . ' ...<p class="text-center"><a class="btn btn-secondary read-more-link" href="' . get_permalink( get_the_ID() ) . '">' . __( 'Read More...',
                Theme\SHORTNAME ) . '</a></p>';
        }

        public function change_admin_footer() {
            echo 'Built for you by <a href="http://detale.ca/" target="_blank">Detale Creative Agency</a>';
        }

        /**
         * Removes the ... from the excerpt read more link
         *
         * @param  string   $more The excerpt.
         * @return string
         */
        public function custom_excerpt_more( $more ) {
            return '';
        }

        public function custom_fields_work() {
            remove_post_type_support( 'location', 'custom-fields' );
        }

        //TinyMCE button changes
        public function edit_buttons( $buttons ) {
            $buttons_to_remove = ['wp_more', 'blockquote', 'charmap', 'wp_help'];
            $buttons           = array_diff( $buttons, $buttons_to_remove );

            return $buttons;
        }

        public function enabled_featured_images() {
            add_theme_support( 'post-thumbnails' );
            add_post_type_support( 'page', 'excerpt' );
        }

        public function favicon_add( $type = 'colour', $prefix = '' ) {
            if ( '' === $prefix ) {
                $prefix = Theme\URI . 'assets/images';
            }

            $output = '';

            $favicon = '/favicon.png';
            if ( 'bw' === $type ) {
                $favicon = '/favicon-admin.png';
            }

            $output .= '<link rel="icon" type="image/png" sizes="32x32" href="' . $prefix . $favicon . '">';

            return $output;
        }

        public function favicon_add_admin() {
            echo $this->favicon_add( 'bw', '' );
        }

        public function favicon_add_main() {
            echo $this->favicon_add( 'colour', '' );
        }

        // Remove p tags if only content is an image
        public function filter_ptags_on_images( $content ) {
            // find all p tags that have just<p>maybe some white space<img all stuff up to /> then maybe whitespace </p> replace it with just the image tag...
            $content = preg_replace( '/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content );

            return preg_replace( '/<p>\s*(<iframe .*>*.<\/iframe>)\s*<\/p>/iU', '\1', $content );
        }

        public function loginURL() {
            return get_bloginfo( 'url' );
        }

        //Login page logo alt
        public function loginURLtext() {
            return get_bloginfo( 'description' );
        }

        public function publish_new_post() {
            delete_transient( 'category_list' );
        }

        public function remove_jpcf_nonadmin() {
            remove_action( 'media_buttons', 'grunion_media_button', 999 );
        }

        public function remove_pods_shortcode_button() {
            if ( class_exists( \PodsInit::class ) ) {
                remove_action( 'media_buttons', [\PodsInit::$admin, 'media_button'], 12 );
            }
        }

        public function scripts() {
            wp_enqueue_style( 'ec-fontawesome', Theme\FONTAWESOME );
        }

        // Favicons
        public function setup_favicons() {
            add_action( 'wp_head', [$this, 'favicon_add_main'] );
            add_action( 'login_head', [$this, 'favicon_add_admin'] );
            add_action( 'admin_head', [$this, 'favicon_add_admin'] );
        }

        // Replace welcome message with building svg on login page
        public function setup_login() {
            add_action( 'login_enqueue_scripts', [$this, 'add_login_style'] );
            add_filter( 'login_headerurl', [$this, 'loginURL'] );
            //add_filter('login_message', [$this, 'login_svg']);
        }

        public function setup_tinymce() {
            // Tinymce Buttons
            add_filter( 'mce_buttons', [$this, 'edit_buttons'] );
            add_filter( 'mce_buttons_2', [$this, 'edit_buttons'] );

            // Edit extra bar buttons
            add_action( 'admin_init', [$this, 'remove_jpcf_nonadmin'] );
            add_action( 'admin_init', [$this, 'remove_pods_shortcode_button'], 14 );
        }
    }
}
