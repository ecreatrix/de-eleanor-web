<?php
/**
 * Inspired by Simon Bradburys cleanup.php fromb4st theme https://github.com/SimonPadbury/b4st
 * Removes the generator tag with WP version numbers. Hackers will use this to find weak and old WP installs
 *
 * @return string
 */
namespace eC\Theme\WP;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Security::class ) ) {
    class Security {
        public function __construct() {
            // Remove Windows Live Writer from header
            remove_action( 'wp_head', 'wlwmanifest_link' );

            // Hide Really Simple Discovery services such as pingbacks
            remove_action( 'wp_head', 'rsd_link' );

            // Hide unnecessary failed login information
            add_filter( 'login_errors', [$this, 'wrong_login'] );

            add_filter( 'the_generator', [$this, 'no_generator'] );
            add_filter( 'login_errors', [$this, 'show_less_login_info'] );

            $this->wp_head();
        }

        // Removes the generator tag with WP version numbers. Hackers will use this to find weak and old WP installs https://github.com/SimonPadbury/b4st
        public function no_generator() {
            return '';
        }

        /**
         * Show less info to users on failed login for security.
         * (Will not let a valid username be known.)
         *
         * @return string
         */
        public function show_less_login_info() {
            return '<strong>ERROR</strong>: Stop guessing!';
        }

        /*
        Clean up wp_head() from unused or unsecure stuff
         */
        public function wp_head{
            remove_action( 'wp_head', 'wp_generator' );
            remove_action( 'wp_head', 'rsd_link' );
            remove_action( 'wp_head', 'wlwmanifest_link' );
            remove_action( 'wp_head', 'index_rel_link' );
            remove_action( 'wp_head', 'feed_links', 2 );
            remove_action( 'wp_head', 'feed_links_extra', 3 );
            remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10 );
            remove_action( 'wp_head', 'wp_shortlink_wp_head', 10 );
        }

        public function wrong_login() {
            return 'Wrong username or password.';
        }
    }
}
