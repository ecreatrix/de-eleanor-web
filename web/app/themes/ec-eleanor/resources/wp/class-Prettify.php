<?php
namespace eC\Theme\WP;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
if ( ! class_exists( Prettify::class ) ) {
    class Prettify {
        public function __construct() {
            //Remove extra dashboard sidebar links
            add_action( 'admin_init', [$this, 'simplify_admin'] );

            add_action( 'wp_before_admin_bar_render', [$this, 'remove_admin_bar_links'] );

            // Move media down below separator
            add_action( 'admin_init', [$this, 'add_admin_menu_separator'] );
            add_action( 'admin_menu', [$this, 'set_admin_menu_separator'] );

            add_filter( 'custom_menu_order', '__return_true' );
            add_filter( 'menu_order', [$this, 'admin_menu_items'] );
        }

        public function add_admin_menu_separator( $position ) {
            global $menu;

            $menu[$position] = [
                0 => '',
                1 => 'read',
                2 => 'separator' . $position,
                3 => '',
                4 => 'wp-menu-separator',
            ];
        }

        public function admin_menu_items( $menu_order ) {
            if ( ! is_array( $menu_order ) ) {
                return $menu_order;
            }

            // Find MonsterInsights link
            //$monster_index = array_search( 'monsterinsights_settings', $menu_order );
            //$monster       = array_splice( $menu_order, $monster_index, 1 );
            //array_splice( $menu_order, 2, 0, $monster );

            // Find media link
            $media_index = array_search( 'upload.php', $menu_order );

            // Find page link
            $page_index = array_search( 'edit.php?post_type=page', $menu_order );

            // Remove media link
            $media = array_splice( $menu_order, $media_index, 1 );

            // Add media link before page
            array_splice( $menu_order, $page_index - 1, 0, $media );

            return $menu_order;
        }

        public function remove_admin_bar_links() {
            global $wp_admin_bar;
            //var_dump($wp_admin_bar);

            $wp_admin_bar->remove_menu( 'wp-logo' );        // Remove the Wordpress logo
            $wp_admin_bar->remove_menu( 'about' );          // Remove the about Wordpress link
            $wp_admin_bar->remove_menu( 'wporg' );          // Remove the Wordpress.org link
            $wp_admin_bar->remove_menu( 'documentation' );  // Remove the Wordpress documentation link
            $wp_admin_bar->remove_menu( 'support-forums' ); // Remove the support forums link
            $wp_admin_bar->remove_menu( 'feedback' );       // Remove the feedback link
            $wp_admin_bar->remove_menu( 'updates' );        // Remove the updates link
            $wp_admin_bar->remove_menu( 'comments' );       // Remove the comments link
            $wp_admin_bar->remove_menu( 'new-content' );    // Remove the content link
            $wp_admin_bar->remove_menu( 'w3tc' );           // If you use w3 total cache remove the performance link

            //$wp_admin_bar->remove_menu('my-account'); // Remove the user details tab

            $wp_admin_bar->remove_menu( 'customize' );

            $wp_admin_bar->remove_menu( 'wpseo-menu' );
        }

        public function set_admin_menu_separator() {
            // Add space after last CPT
            do_action( 'admin_init', 15 );
        }

        //Choose if user needs to see extra pages
        public function show_menus() {
            $show_menus = get_user_meta( get_current_user_id(), 'ec_show_menus', true );

            return $show_menus;
        }

        public function simplify_admin() {
            //Current user is not me so hide extra menu items
            $show_menus = $this->show_menus();
            //$show_menus = false;

            $this->simplify_basic( $show_menus );
            $this->simplify_plugins( $show_menus );
            $this->simplify_advanced( $show_menus );
        }

        //Advanced functions that aren't needed for the average user
        public function simplify_advanced( $show_menus ) {
            if ( $show_menus ) {
                return;
            }

            remove_menu_page( 'Wordfence' );
            remove_menu_page( 'wpcf7' );
            remove_menu_page( 'wpseo_dashboard' );

            remove_menu_page( 'user-new.php' );

            remove_submenu_page( 'themes.php', 'nav-menus.php' );
            remove_submenu_page( 'users.php', 'user-new.php' );
            remove_menu_page( 'update-core.php' );

            remove_menu_page( 'tools.php' );

            remove_menu_page( 'options-general.php' );
            remove_menu_page( 'pods' );
            remove_menu_page( 'tools.php' );
            remove_menu_page( 'plugins.php' );
            remove_menu_page( 'themes.php' );

            remove_submenu_page( 'themes.php', 'widgets.php' );

            if ( class_exists( 'Jetpack' ) ) {
                remove_menu_page( 'jetpack' );
            }
        }

        //Generic WP sections that are not needed
        public function simplify_basic( $show_menus ) {
            remove_menu_page( 'edit.php' );
            remove_menu_page( 'link-manager.php' );
            remove_menu_page( 'edit-comments.php' );
        }

        //Plugins that don't seem to be used but included in prebuilt theme or don't need customization
        public function simplify_plugins( $show_menus ) {
            remove_menu_page( 'wp-user-avatar' );

            remove_menu_page( 'theme-editor.php' );
            remove_menu_page( 'customize.php' );

            remove_menu_page( 'piklist-core-addons' );
            remove_menu_page( 'piklist' );

            remove_submenu_page( 'themes.php', 'theme-editor.php' );
        }
    }
}
