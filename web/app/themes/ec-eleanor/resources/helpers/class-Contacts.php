<?php
//Social media, phone number, email, ...
namespace eC\Theme\Helpers;

// Exit if accessed directly.
if (!defined('ABSPATH')) {exit;}
if (!class_exists(Contacts::class)) {
    class Contacts
    {
        public function __construct()
        {
        }

        public static function add_social_media($parent_class, $link_class = '', $social_links_order = '')
        {
            $company_info = get_option('ec_company_info_settings', '');

            if (!$company_info || !array_key_exists('ec_company_socialmedia', $company_info)) {
                return '';
            }
            $social_links = $company_info['ec_company_socialmedia'];

            // Stop if no values are present
            if (!is_array($social_links)) {
                return '';
            }

            if ('' === $social_links_order) {
                $social_links_order = [
                    ['linkedin', '-square'],
                    ['instagram', ''],
                    ['twitter', '-square'],
                    ['facebook', '-square'],
                    ['youtube', '-square'],
                    ['google-plus', '-square'],
                    ['pinterest', '-square'],
                    ['rss', '-square']
                ];
            }

            $output = '';
            foreach ($social_links_order as $key) {
                if (key_exists($key[0], $social_links) && !empty($social_links[$key[0]])) {
                    $class = trim('nav-link link-' . $key[0] . ' ' . $link_class);
                    $output .= '<li class="nav-item"><a class="' . $class . '" href="' . $social_links[$key[0]] . '" target="_blank"' . '><i class="fa fa-' . $key[0] . $key[1] . '"></i>';

                    $output .= '</a></li>';
                }
            }

            $ul_classes = trim('social-media ' . $parent_class);

            return '<ul class="' . $ul_classes . '">' . $output . '</ul>';
        }

        //Email/tel html 5 links
        public static function email($email)
        {
            return '<a href="mailto:' . $email . '">' . $email . '</a>';
        }

        public static function tel($tel)
        {
            return '<a href="tel:1+' . $tel . '">' . $tel . '</a>';
        }
    }
}
