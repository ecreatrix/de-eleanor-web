<?php
/*
Name: SVG Upload
Description: Super PHP Plugin to add Full SVG Media support to WordPress
Author: Lewis Cowles
Version: 1.5.2
Author URI: http://www.lewiscowles.co.uk/
GitHub Plugin URI: Lewiscowles1986/WordPressSVGPlugin
 */

namespace eC\Theme\Helpers;

use \eC\Theme as Theme;

// Exit if accessed directly.
if (!defined('ABSPATH')) {exit;}

if (!class_exists(SVGSupport::class)) {
    class SVGSupport
    {
        public function __construct()
        {
            add_action('admin_init', [$this, 'add_svg_upload']);
            add_action('load-post.php', [$this, 'add_editor_styles']);
            add_action('load-post-new.php', [$this, 'add_editor_styles']);
            // forced crop STFU
            add_action('after_setup_theme', [$this, 'theme_prefix_setup'], 99);
            add_filter('upload_mimes', [$this, 'filter_mimes']);

            add_filter('wp_check_filetype_and_ext', [$this, 'svg_update_fix'], 10, 4);

        }

        public function add_editor_styles()
        {
            add_filter('mce_css', [$this, 'filter_mce_css']);
        }

        public function add_svg_upload()
        {
            ob_start();

            add_action('wp_ajax_adminlc_mce_svg.css', [$this, 'tinyMCE_svg_css']);
            add_filter('image_send_to_editor', [$this, 'remove_dimensions_svg'], 10);
            add_action('shutdown', [$this, 'on_shutdown'], 0);
            add_filter('final_output', [$this, 'fix_template']);
        }

        public static function clean_svg($svg)
        {
            // Load the dirty svg
            $dirtySVG = file_get_contents($svg);

            // Stop if Sanitizer class doesn't exist
            if (!class_exists(\enshrined\svgSanitize\Sanitizer::class)) {
                return $dirtySVG;
            }

            // Create a new sanitizer instance
            $sanitizer = new \enshrined\svgSanitize\Sanitizer();

            // Pass it to the sanitizer and get it back clean

            return $sanitizer->sanitize($dirtySVG);
        }

        public function filter_mce_css($mce_css)
        {
            global $current_screen;
            $mce_css .= ', ' . '/wp-admin/admin-ajax.php?action=adminlc_mce_svg.css';

            return $mce_css;
        }

        public function filter_mimes($mimes = [])
        {
            $mimes['svg']  = 'image/svg+xml';
            $mimes['svgz'] = 'image/svg+xml';

            return $mimes;
        }

        public function fix_template($content = '')
        {
            $content = str_replace(
                '<# } else if ( \'image\' === data.type && data.sizes && data.sizes.full ) { #>',
                '<# } else if ( \'svg+xml\' === data.subtype ) { #>
                <img class="details-image" src="{{ data.url }}" draggable="false" />
            <# } else if ( \'image\' === data.type && data.sizes && data.sizes.full ) { #>',
                $content
            );
            $content = str_replace(
                '<# } else if ( \'image\' === data.type && data.sizes ) { #>',
                '<# } else if ( \'svg+xml\' === data.subtype ) { #>
                <div class="centered">
                    <img src="{{ data.url }}" class="thumbnail" draggable="false" />
                </div>
            <# } else if ( \'image\' === data.type && data.sizes ) { #>',
                $content
            );

            return $content;
        }

        public function media_mime_types($mimes)
        {
            $mimes['svg'] = 'image/svg+xml';

            return $mimes;
        }

        public function on_shutdown()
        {
            $final     = '';
            $ob_levels = count(ob_get_level());
            for ($i = 0; $i < $ob_levels; $i++) {
                $final .= ob_get_clean();
            }
            echo apply_filters('final_output', $final);
        }

        public function remove_dimensions_svg($html = '')
        {
            return str_ireplace([" width=\"1\"", " height=\"1\""], "", $html);
        }

        //Return svg of file if it exists, if not return png if it exists. If neither do, return false
        public static function svg_or_png_return($path = Theme\PATH, $uri = Theme\URI, $file = 'logo', $directory = 'images')
        {
            $created_path = $path . $directory . '/' . $file;
            $created_link = $uri . $directory . '/' . $file;

            if (file_exists($file)) {
                $filetype = mime_content_type($file);

                if (!$directory && 'image/svg' === $filetype) {
                    //Return svg file from uploads

                    return self::clean_svg($file);
                } else if (!$directory && $file) {
                    //Return image file from uploads

                    global $wpdb;
                    $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $file));

                    return $attachment[0];
                }
            } else if (file_exists($created_path . '.svg')) {
                //Return svg file from theme folders

                return self::clean_svg($created_path . '.svg');
            } else if (file_exists($created_path . '.png')) {
                //Return image file from theme folders
                list($width, $height) = getimagesize($created_path . '.png');

                return '<img src="' . $created_link . '.png" alt="Site Logo" width="' . $width . '" height="' . $height . '" />';
            }

            return '';
        }

        public function svg_update_fix($data, $file, $filename, $mimes)
        {
            global $wp_version;
            if ('4.7.1' !== $wp_version) {
                return $data;
            }

            $filetype = wp_check_filetype($filename, $mimes);

            return [
                'ext'             => $filetype['ext'],
                'type'            => $filetype['type'],
                'proper_filename' => $data['proper_filename'],
            ];

        }

        public function theme_prefix_setup()
        {
            $existing = get_theme_support('custom-logo');
            if ($existing) {
                $existing                = current($existing);
                $existing['flex-width']  = true;
                $existing['flex-height'] = true;
                add_theme_support('custom-logo', $existing);
            }
        }

        public function tinyMCE_svg_css()
        {
            header('Content-type: text/css');
            echo 'img[src$=".svg"] { width: 100%; height: auto; }';
            exit();
        }
    }
/*
 * Logic Breakdown
 * 1) We need the whole page, but only if we are in the backend (hence admin_init hook)
 * 2) We would like to grab all output (ob_start within admin_init as nothing should echo before that, we should not interfere with things that do)
 * 3) We want to grab the content on shutdown, concatenate all output buffers, then filter
 * 4) Search for placeholders which should exist and replace the text
 * Downsides
 * 1) Not permanent fix (for perma fix WP core would need to be editable or native filtering added)
 * 2) A bit resource munchy (it's locked to the admin side, so IMHO who cares)
 * 3) This is just to get SVG into WP core... Luckily the find replace is that simple in /wp-includes/media-template.php (Patch it Mullweng & Co!)
 *
 * Changes...
 * Re-factored function declaration and calls to make compatible with lesser PHP versions, despite believing anyone using such versions is dangerous
 * Added param for mime-types to filter_mimes function
 * Moved into a namespace and class to make the whole thing less hack-and-slash
 * Updated to use short-array syntax (Breaking change update your PHP or don't use)
 * Deleted some dead code I never noticed before
 */
}
