<?php
namespace eC\Theme\Posts;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( Setup::class ) ) {
    class Setup {
        public function __construct() {
            $this->helpers();

            // For testing
            //add_action( 'admin_head', [$this, 'debug'] );
        }

        public function debug() {
            global $pagenow;

            print_r( $pagenow );
            echo '<br>';

            print_r( $_GET['taxonomy'] );
            echo '<br>';

            $current_screen = get_current_screen();

            print_r( $current_screen->id );
        }

        public function helpers() {
            if ( class_exists( Helpers::class ) ) {
                $helpers = new Helpers();
            }
        }
    }
}
