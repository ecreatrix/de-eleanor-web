<?php
namespace eC\Theme\Posts;

use eC\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( Helpers::class ) ) {
    class Helpers {
        public function __construct() {
            // Used by multiple post types
        }
    }
}
