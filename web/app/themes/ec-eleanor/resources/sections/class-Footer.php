<?php
namespace eC\Theme\Sections;

use eC\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Footer::class ) ) {
    class Footer {
        public function __construct() {
            add_action( 'ec_footer', [$this, 'footer_content_print'], 10 );
        }

        public function copyright() {
            return '<div class="text-center copywrite"><span class="copy">Copyright &copy; ' . date( 'Y' ) . '</span> <span class="name">' . get_bloginfo( 'name' ) . '</span></div>';
        }

        public function footer_content_print() {
            $copyright = $this->copyright();

            echo '<div class="container">' . $copyright . '</div>';
        }
    }
}