<?php
namespace eC\Theme\Sections;

use eC\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Frontend::class ) ) {
    class Frontend {
        public function __construct() {
            //Add classes/shortcode to frontend body elements
            add_action( 'init', [$this, 'page_excerpts_allow'] );

            // Add default posts and comments RSS feed links to head.
            add_theme_support( 'automatic-feed-links' );

            // Let WordPress manage the document title. By adding theme support, we declare that this theme does not use a hard-coded <title> tag in the document head, and expect WordPress to provide it for us.
            add_theme_support( 'title-tag' );

            //add_filter('document_title_parts', [$this, 'custom_titles'], 10);

            //Add featured image to posts
            add_theme_support( 'post-thumbnails' );

            //add_filter('get_image_tag_class', [$this, 'image_class_filter']);

            // Switch default core markup for search form, comment form, and comments to output valid HTML5.
            add_theme_support( 'html5', [
                'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
            ] );

            add_theme_support( 'align-wide' );
            // Enable support for Post Formats. See http://codex.wordpress.org/Post_Formats
            //add_theme_support('post-formats', [ 'aside', 'image', 'video', 'quote', 'link']);

            add_action( 'ec_body_extras', [$this, 'body_scrollspy'], 10 );
            add_action( 'body_class', [$this, 'body_class'], 10 );

            // Pinterest code
            add_action( 'wp_head', [$this, 'pinterest_meta'] );

            // Progressive loading
            add_action( 'wp_head', function () {
                ob_flush();
                flush();
            }, 999 );
        }

        public function attachments_add_category() {
            //register_taxonomy_for_object_type( 'category', 'attachment' );
            //add_post_type_support('attachment', 'category');
        }

        public function body_class( $classes ) {
            $classes[] = 'article-make-room';
            $classes[] = 'fixed-navbar';

            return $classes;
        }

        public function body_scrollspy() {
            return 'data-spy="scroll" data-target="#collapsing-navbar"';
        }

        public static function categorized_blog() {
            if ( false === ( $all_the_cool_cats = get_transient( 'understrap_categories' ) ) ) {
                // Create an array of all the categories that are attached to posts.
                $all_the_cool_cats = get_categories( [
                    'fields'     => 'ids',
                    'hide_empty' => 1,
                    // We only need to know if there is more than one category.
                    'number'     => 2,
                ] );
                // Count the number of categories that are attached to the posts.
                $all_the_cool_cats = count( $all_the_cool_cats );
                set_transient( 'understrap_categories', $all_the_cool_cats );
            }
            if ( $all_the_cool_cats > 1 ) {
                // This blog has more than 1 category so components_categorized_blog should return true.
                return true;
            } else {
                // This blog has only 1 category so components_categorized_blog should return false.
                return false;
            }
        }

        public function custom_titles( $title ) {
            global $page, $paged;

            if ( empty( $title ) && ( is_home() || is_front_page() ) ) {
                $title = __( 'Home', Theme\SHORTNAME ) . ' | ';
            }

            $name             = get_bloginfo( 'name' );
            $site_description = get_bloginfo( 'description', 'display' );
            if ( $site_description && ( is_home() || is_front_page() ) ) {
                echo " | $site_description";
            }

            // Add a page number if necessary:
            if ( $paged >= 2 || $page >= 2 ) {
                echo ' | ' . sprintf( __( 'Page %s', eC\Theme\SHORTNAME ), max( $paged, $page ) );
            }

            //Check if custom titles are enabled from your option framework
            if ( function_exists( 'pods' ) && is_pod_page() ) {
                //$page_pod       = pods('product', pods_v_sanitized('last', 'url'));
                //$title['title'] = $model_label. ' ' . $page_pod->display('name');
            }

            return $title;
        }

        //Add Bootstrap's img-fluid to all images to make them responsive
        public function image_class_filter( $class, $id, $align, $size ) {
            return 'img-fluid ' . $class;
        }

        public function page_excerpts_allow() {
            add_post_type_support( 'page', 'excerpt' );
        }

        public function pinterest_meta() {
            echo '<meta name="p:domain_verify" content="d9e43d8dd19f488b8ce61fb156135123"/>';
        }

        public static function search_excerpt_highlight() {
            $excerpt = get_the_excerpt();
            $keys    = implode( '|', explode( ' ', get_search_query() ) );
            $excerpt = preg_replace( '/(' . $keys . ')/iu', '<strong class="search-highlight">\0</strong>', $excerpt );

            echo '<p>' . $excerpt . '</p>';
        }

        public static function search_title_highlight() {
            $title = get_the_title();
            $keys  = implode( '|', explode( ' ', get_search_query() ) );
            $title = preg_replace( '/(' . $keys . ')/iu', '<strong class="search-highlight">\0</strong>', $title );

            echo $title;
        }

        public static function single_cats( $show_cats = true, $show_tags = true ) {
            // Hide category and tag text for pages.
            if ( 'post' !== get_post_type() ) {
                return '';
            }

            $output = '';
            /* translators: used between list items, there is a space after the comma */
            $categories_list = get_the_category_list( esc_html__( ', ', 'ec-theme' ) );
            if ( $show_cats && $categories_list && self::categorized_blog() ) {
                $output .= '<span class="cat-links">' . esc_html__( 'Posted in ', 'ec-theme' ) . $categories_list . '</span>'; // WPCS: XSS OK.
            }

            /* translators: used between list items, there is a space after the comma */
            $tags_list = get_the_tag_list( '', esc_html__( ', ', 'ec-theme' ) );
            if ( $show_tags && $tags_list ) {
                $output .= '<span class="tags-links">' . esc_html__( 'Tagged ', 'ec-theme' ) . $tags_list . '</span>'; // WPCS: XSS OK.
            }

            return $output;
        }
    }
}
