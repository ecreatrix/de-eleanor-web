<?php
namespace eC\Theme\Sections;

use eC\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Menus::class ) ) {
    class Menus {
        public function __construct() {
            // Setup top nav settings and footer text
            add_action( 'ec_header', [$this, 'header'], 10 );

            // Remove default WP admin bar css
            add_action( 'header', [$this, 'remove_admin_login_header'] );

            register_nav_menus( $this->register_navs() );
        }

        public function collapse_button() {
            $burger = Theme\Helpers\SVGSupport::svg_or_png_return( Theme\PATH, Theme\URI, 'burger', 'assets/images' );

            if ( '' === $burger ) {
                $burger = '<i class="fa fa-bars" aria-hidden="true"></i>';
            }

            return '<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#collapsing-navbar" aria-controls="collapsing-navbar" aria-expanded="false" aria-label="Toggle navigation">' . $burger . '</button>';
        }

        public function header( $theme_location ) {
            $output = self::nav_create( [] ) . $this->social_media() . $this->collapse_button();

            $output = '<nav class="navbar navbar-expand-md fixed-top navbar-light bg-white"><div class="container">' . apply_filters( 'ec_nav', $output ) . '</div></nav>';

            echo $output;
        }

        // Add site links to menu nav items
        // Input array $atts = ['theme_location', 'menu classes', 'container_id', 'container_classes']
        public static function nav_create( $given_atts ) {
            // Make array to use with nav menu wp function
            $default_atts = [
                'container'       => 'div',
                'container_id'    => 'collapsing-navbar',
                'container_class' => 'navbar-collapse collapse',
                'menu_class'      => 'navbar-nav',

                'depth'           => 2,

                'theme_location'  => 'primary',
                'echo'            => false,
            ];

            $nav_atts = array_merge( $default_atts, $given_atts );

            // Use walker if available
            if ( class_exists( NavWalker::class ) ) {
                $nav_atts['walker'] = new NavWalker();
            }

            return wp_nav_menu( $nav_atts );
        }

        public function register_navs() {
            $menus = [
                'primary' => __( 'Menu', 'ec_theme' ),
            ];

            return $menus;
        }

        public function remove_admin_login_header() {
            remove_action( 'wp_head', '_admin_bar_bump_cb' );
        }

        public function social_media() {
            $company_info = get_option( 'ec_company_info_settings', '' );

            if ( ! $company_info || ! array_key_exists( 'ec_company_socialmedia', $company_info ) ) {
                return '';
            }
            $social_links = $company_info['ec_company_socialmedia'];

            // Stop if no values are present
            if ( ! is_array( $social_links ) ) {
                return '';
            }

            $social_links_order = [
                ['linkedin', ''],
                ['instagram', ''],
                ['twitter', ''],
                ['facebook', ''],
                ['youtube', ''],
                ['google-plus', ''],
                ['pinterest', ''],
                ['rss', ''],
            ];

            $output = '';
            foreach ( $social_links_order as $key ) {
                if ( array_key_exists( $key[0], $social_links ) && ! empty( $social_links[$key[0]] ) ) {
                    $class = trim( 'nav-link link-' . $key[0] . ' ' . $link_class );
                    $output .= '<li class="nav-item"><a class="' . $class . '" href="' . $social_links[$key[0]] . '" target="_blank"' . '><i class="fab fa-' . $key[0] . $key[1] . '"></i>';

                    $output .= '</a></li>';
                }
            }

            return '<ul class="social-media navbar-nav">' . $output . '</ul>';
        }
    }
}
