<?php
/**
 * Theme Functions with class loading
 *
 */

namespace eC\Theme;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    exit;
}

if ( ! class_exists( Build::class ) ) {
    // Get the theme data.
    $the_theme = wp_get_theme();

    defined( __NAMESPACE__ . '\VERSION' ) or define( __NAMESPACE__ . '\VERSION', $the_theme->get( 'Version' ) );

    defined( __NAMESPACE__ . '\URI' ) or define( __NAMESPACE__ . '\URI', get_template_directory_uri() . '/' );
    defined( __NAMESPACE__ . '\PATH' ) or define( __NAMESPACE__ . '\PATH', get_template_directory() . '/' );

    defined( __NAMESPACE__ . '\NAME' ) or define( __NAMESPACE__ . '\NAME', 'Eleanor' );
    defined( __NAMESPACE__ . '\SHORTNAME' ) or define( __NAMESPACE__ . '\SHORTNAME', 'ec-theme-' );
    defined( __NAMESPACE__ . '\CODENAME' ) or define( __NAMESPACE__ . '\CODENAME', 'ec_theme_' );
    defined( __NAMESPACE__ . '\CAMEL' ) or define( __NAMESPACE__ . '\CAMEL', 'ecTheme' );
    defined( __NAMESPACE__ . '\BLOCK' ) or define( __NAMESPACE__ . '\BLOCK', 'ectheme' );

    defined( __NAMESPACE__ . '\FONTAWESOME' ) or define( __NAMESPACE__ . '\FONTAWESOME', 'https://use.fontawesome.com/0a60f349fc.js' );

    if ( file_exists( get_template_directory() . '/resources/includes/autoload.php' ) ) {
        require_once get_template_directory() . '/resources/includes/autoload.php';

        spl_autoload_register( function ( $class ) {
            \eC\loader( $class, get_template_directory() . '/resources/', 'eC\\Theme\\' );
        } );
    }

    class Build {
        // Holds the main instance.
        private static $__instance = null;

        public function __construct() {
            $this->init_classes();
        }

        public function init_classes() {
            $this->init_classes_scripts();

            $this->init_classes_helpers();

            $this->init_classes_wp();
            $this->init_classes_gutenberg();
            $this->init_classes_content();

            $this->init_classes_plugins();

            $this->init_classes_settings();
        }

        // WP
        public function init_classes_Gutenberg() {
            if ( class_exists( Gutenberg\Setup::class ) ) {
                new Gutenberg\Setup();
            }
        }

        public function init_classes_content() {
            // Content
            if ( class_exists( Sections\Menus::class ) ) {
                new Sections\Menus();
            }

            if ( class_exists( Sections\Frontend::class ) ) {
                new Sections\Frontend();
            }

            if ( class_exists( Sections\Footer::class ) ) {
                new Sections\Footer();
            }
        }

        public function init_classes_helpers() {
            if ( class_exists( Helpers\SVGSupport::class ) ) {
                new Helpers\SVGSupport();
            }
        }

        public function init_classes_plugins() {
            if ( class_exists( Plugins\Piklist::class ) ) {
                new Plugins\Piklist();
            }

            if ( class_exists( 'Jetpack' ) && class_exists( Plugins\Jetpack::class ) ) {
                new Plugins\Jetpack();
            }

            if ( class_exists( 'Soil' ) && class_exists( Plugins\Soil::class ) ) {
                new Plugins\Soil();
            }
        }

        // Functional
        public function init_classes_scripts() {
            if ( class_exists( Scripts\Setup::class ) ) {
                new Scripts\Setup();
            }
        }

        public function init_classes_settings() {
            //if ( class_exists( Settings\Setup::class ) ) {
            //    new Settings\Setup();
            //}
        }

        // WP
        public function init_classes_wp() {
            if ( class_exists( WP\Changes::class ) ) {
                new WP\Changes();
            }

            if ( class_exists( WP\Prettify::class ) ) {
                new WP\Prettify();
            }

            if ( class_exists( WP\Settings::class ) ) {
                new WP\Settings();
            }

            if ( class_exists( Posts\Setup::class ) ) {
                new Posts\Setup();
            }
        }

        // Get/create the plugin instance.
        public static function instance() {
            if ( empty( self::$_instance ) ) {
                self::$__instance = new Build();
            }

            return self::$__instance;
        }

        public function setup_wordpress() {
        }
    }

    Build::instance();
}
