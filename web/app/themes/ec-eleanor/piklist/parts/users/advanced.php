<?php
/*
Title: Advanced
 */

piklist('field', [
    'type'        => 'checkbox',
    'field'       => 'ec_show_menus',
    'label'       => __('Dashboard view'),
    'description' => __('Show advanced dashboard with more menu items'),
    //'scope'       => 'user_meta',
    'choices'     => [
        true => 'Show advanced menus'
    ]
    //'value'       => false
]);
