<?php
/*
 * Setting: ec_company_info_settings
 */

//Contact info

//Social Media
piklist( 'field', [
    'type'   => 'group',
    'field'  => 'ec_company_socialmedia', // removing this parameter saves all fields as separate meta
    'label'  => __( 'Social Media', 'ec-theme' ),
    'list'   => false,
    //'description' => __('A grouped field with the field parameter set.', 'ec-theme'),
    'fields' => [
        [
            'type'       => 'url',
            'field'      => 'linkedin',
            'label'      => __( 'LinkedIn', 'ec-theme' ),
            'columns'    => 12,
            'attributes' => [
                'placeholder' => 'https://www.linkedin.com',
            ],
        ],
        [
            'type'       => 'url',
            'field'      => 'instagram',
            'label'      => __( 'Instagram', 'ec-theme' ),
            'columns'    => 12,
            'attributes' => [
                'placeholder' => 'http://instagram.com',
            ],
        ],
        [
            'type'       => 'url',
            'field'      => 'twitter',
            'label'      => __( 'Twitter', 'ec-theme' ),
            'columns'    => 12,
            'attributes' => [
                'placeholder' => 'https://twitter.com',
            ],
        ],
        [
            'type'       => 'url',
            'field'      => 'youtube',
            'label'      => __( 'YouTube', 'ec-theme' ),
            'columns'    => 12,
            'attributes' => [
                'placeholder' => 'https://youtube.com',
            ],
        ],
        [
            'type'       => 'url',
            'field'      => 'facebook',
            'label'      => __( 'Facebook', 'ec-theme' ),
            'columns'    => 12,
            'attributes' => [
                'placeholder' => 'https://www.facebook.com',
            ],
        ],
    ],
] );
