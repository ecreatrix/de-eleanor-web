<?php
/**
 * The template for displaying search results pages.
 *
 */

?>

<form role="search" method="get" class="search-form" action="<?php echo home_url('/'); ?>">
	<div class="input-group">
      <input type="text" class="form-control" placeholder="">
      <span class="input-group-btn">
        <button class="btn" type="button"><span class='fa fa-search fa-flip-horizontal'></span></button>
      </span>
    </div>
</form>
