<?php
/**
 * The template for displaying all single posts.
 *
 */

use \eC\Theme as Theme;

get_header();

do_action('ec_before_page');

while (have_posts()) {
    the_post();

    get_template_part('partials/content-single');

    //TODO allow comments
    // If comments are open or we have at least one comment, load up the comment template
    if (comments_open() || '0' != get_comments_number()) {
        //comments_template();
    }
}
get_sidebar();

get_footer();
