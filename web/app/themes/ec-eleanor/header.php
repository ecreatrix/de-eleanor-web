<!DOCTYPE html><?php
	/**
	 * Theme header
	 *
	 * Displays all of the <head> section and everything up till <div id="content">
	 *
	 */
?>
<html<?php language_attributes(); ?>>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> -<?php bloginfo( 'description' ); ?>">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<script>document.cookie='resolution='+Math.max(screen.width,screen.height)+("devicePixelRatio" in window ? ","+devicePixelRatio : ",1")+'; path=/';</script>
<?php wp_head(); ?>
</head>

<?php
	$body_extras = apply_filters( 'ec_body_extras', '' );
?>

<body           <?php do_action( 'ec_body_id' );
           body_class();
           echo $body_extras; ?>>

	<div class="site-content">
<?php

do_action( 'ec_before_header' ); ?>
	<header id="masthead" class="site-header">
		<?php do_action( 'ec_header' ); ?>
	</header><!-- #masthead -->
	<?php do_action( 'ec_after_header' ); ?>

<article id="post-<?php the_ID(); ?>"<?php post_class(); ?>>
<?php do_action( 'ec_before_article' ); ?>
