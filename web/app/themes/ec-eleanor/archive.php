<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */
use \eC\Theme as Theme;

get_header();

if ( have_posts() ) {
    echo '<div class="jumbotron archive"><div class="container"><h1 class="h3 page-title">';

    if ( is_category() ) {
        single_cat_title();
    } else if ( is_tag() ) {
        single_tag_title();
    } else if ( is_author() ) {
        printf( __( 'Author: %s', Theme\SHORTNAME ), '<span class="vcard">' . get_the_author() . '</span>' );
    } else if ( is_day() ) {
        printf( __( 'Day: %s', Theme\SHORTNAME ), '<span>' . get_the_date() . '</span>' );
    } else if ( is_month() ) {
        printf( __( 'Month: %s', Theme\SHORTNAME ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', Theme\SHORTNAME ) ) . '</span>' );
    } else if ( is_year() ) {
        printf( __( 'Year: %s', Theme\SHORTNAME ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', Theme\SHORTNAME ) ) . '</span>' );
    } else {
        _e( 'Archives', Theme\SHORTNAME );
    }

    echo '</h1>';

    // Show an optional term description.
    $term_description = term_description();

    if ( ! empty( $term_description ) ) {
        printf( '<div class="taxonomy-description">%s</div>', $term_description );
    }
    while ( have_posts() ) {
        the_post();
        echo '<div class="post post-' . get_the_id() . '">';

        /* Include the Post-Format-specific template for the content.
         * If you want to override this in a child theme, then include a file
         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
         */
        get_template_part( 'partials/content', get_post_format() );
        echo '</div>';
    }

    echo '</div></div>';
} else {
    get_template_part( 'partials/content', 'none' );
}

get_sidebar();
get_footer();
