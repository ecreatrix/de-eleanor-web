<?php
/**
 * The main template file.
 *
 */

get_header();

use \eC\Theme as Theme;

do_action( 'ec_before_page' );

$page_id = get_option( 'page_for_posts' );
if ( have_posts() ) {
    the_content();
} else {
    include locate_template( 'partials/content-none.php' );
}

get_sidebar();

get_footer();
