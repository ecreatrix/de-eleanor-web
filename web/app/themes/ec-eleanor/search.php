<?php
/**
 * The template for displaying search results pages.
 *
 */

use \eC\Theme as Theme;

get_header();?>


<?php if (have_posts()): ?>

<?php do_action('ec_before_page');?>
<div class="jumbotron"><div class="container">
	<h1 class="page-title mb-6"><?php printf(__('Search Results for: %s', Theme\SHORTNAME), '<span>' . get_search_query() . '</span>');?></h1>

<?php while (have_posts()): the_post();
// Run the loop for the search to output the results.
    get_template_part('partials/content', 'search');
endwhile;
echo '</div></div>';

else:
    get_template_part('partials/content', 'none');

endif;

get_sidebar();
get_footer();
